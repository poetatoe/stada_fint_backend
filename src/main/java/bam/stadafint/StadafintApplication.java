package bam.stadafint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StadafintApplication {

    public static void main(String[] args) {
        SpringApplication.run(StadafintApplication.class, args);
    }

}
